package ru.frosteye.ovsa.commons.storage.serialization

interface Serializer {

    fun serialize(any: Any?): String

    fun <T> deserialize(string: String?, typeOfT: Class<T>): T?
}

