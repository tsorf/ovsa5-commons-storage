package ru.frosteye.ovsa.commons.storage.persisted

interface Storage {

    fun set(key: String, value: Any?)
    fun setPersisted(value: Any)

    fun getString(key: String): String?
    fun getLong(key: String): Long
    fun getInt(key: String): Int
    fun getBoolean(key: String): Boolean
    fun getFloat(key: String): Float

    fun transaction(action: (Editor) -> Unit)

    /**
     * Небезопасно для использования в Observable,
     * поскольку возвращает nullable-результат
     */
    fun <T> getPersisted(type: Class<T>): T?
    fun clearPersisted(type: Class<*>)

    interface Editor {

        fun set(key: String, value: Any?)
        fun setPersisted(value: Any)
        fun clearPersisted(type: Class<*>)
    }
}