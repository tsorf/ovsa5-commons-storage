package ru.frosteye.ovsa.commons.storage

import ru.frosteye.ovsa.commons.storage.persisted.Persisted
import java.lang.IllegalArgumentException
import kotlin.reflect.KClass

fun Class<*>.persistedName(): String {
    return getAnnotation(Persisted::class.java)?.storageKey
        ?: throw IllegalArgumentException("$this must be annotated as @Persisted")
}

fun KClass<*>.persistedName(): String {
    return java.persistedName()
}