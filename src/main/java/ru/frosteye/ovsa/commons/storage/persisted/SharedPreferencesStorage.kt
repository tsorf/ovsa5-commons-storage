package ru.frosteye.ovsa.commons.storage.persisted

import android.annotation.SuppressLint
import android.content.SharedPreferences
import ru.frosteye.ovsa.commons.storage.serialization.Serializer
import ru.frosteye.ovsa.commons.storage.persistedName
import java.lang.Exception
import javax.inject.Inject

class SharedPreferencesStorage @Inject constructor(
    private val preferences: SharedPreferences,
    private val serializer: Serializer
) : Storage {

    @SuppressLint("CommitPrefEdits")
    override fun set(key: String, value: Any?) {
        val editor = EditorImpl(preferences.edit())
        editor.set(key, value)
        editor.apply()
    }

    @SuppressLint("CommitPrefEdits")
    override fun setPersisted(value: Any) {
        val editor = EditorImpl(preferences.edit())
        editor.setPersisted(value)
        editor.apply()
    }

    @SuppressLint("CommitPrefEdits")
    override fun clearPersisted(type: Class<*>) {
        val editor = EditorImpl(preferences.edit())
        editor.set(type.persistedName(), null)
        editor.apply()
    }

    override fun getString(key: String): String? {
        return preferences.getString(key, null)
    }

    override fun getLong(key: String): Long {
        return preferences.getLong(key, 0)
    }

    override fun getInt(key: String): Int {
        return preferences.getInt(key, 0)
    }

    override fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    override fun getFloat(key: String): Float {
        return preferences.getFloat(key, 0f)
    }

    @SuppressLint("CommitPrefEdits")
    override fun transaction(action: (Storage.Editor) -> Unit) {
        val editor = EditorImpl(preferences.edit())
        action(editor)
        editor.apply()
    }

    override fun <T> getPersisted(type: Class<T>): T? {
        val raw = preferences.getString(type.persistedName(), null)
            ?: return null
        return try {
            serializer.deserialize(raw, type)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    inner class EditorImpl(
        private val preferencesEditor: SharedPreferences.Editor
    ) : Storage.Editor {


        private fun putAny(key: String, value: Any?): EditorImpl {
            if (value != null) {
                preferencesEditor.putString(key, serializer.serialize(value))
            } else {
                preferencesEditor.putString(key, null)
            }
            return this
        }

        internal fun apply() {
            preferencesEditor.apply()
        }

        override fun set(key: String, value: Any?) {
            if (value == null) {
                preferencesEditor.putString(key, null)
                return
            }
            when (value) {
                is String -> preferencesEditor.putString(key, value)
                is Long -> preferencesEditor.putLong(key, value)
                is Int -> preferencesEditor.putInt(key, value)
                is Boolean -> preferencesEditor.putBoolean(key, value)
                is Float -> preferencesEditor.putFloat(key, value)
                else -> putAny(key, value)
            }
        }

        override fun setPersisted(value: Any) {
            val key = value::class.java.persistedName()
            preferencesEditor.putString(key, serializer.serialize(value))
        }

        override fun clearPersisted(type: Class<*>) {
            val key = type.persistedName()
            preferencesEditor.putString(key, null)
        }
    }

}