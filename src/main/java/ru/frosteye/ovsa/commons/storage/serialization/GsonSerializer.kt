package ru.frosteye.ovsa.commons.storage.serialization

import com.google.gson.Gson
import com.google.gson.GsonBuilder

class GsonSerializer(
    private val gson: Gson
) : Serializer {

    constructor() : this(GsonBuilder().create())

    override fun serialize(any: Any?): String {
        return gson.toJson(any)
    }

    override fun <T> deserialize(string: String?, typeOfT: Class<T>): T? {
        return gson.fromJson(string, typeOfT)
    }
}