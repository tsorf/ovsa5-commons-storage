package ru.frosteye.ovsa.commons.storage.persisted

import android.content.SharedPreferences

/**
 * Аннотация для моделей, которые можно хранить в [SharedPreferences] в виде json
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Persisted(
    val storageKey: String
)